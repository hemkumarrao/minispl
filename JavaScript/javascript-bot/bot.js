/*
 * bot.js
 *
 * Main logic of the bot resides in this file.
 *
 * This Bot class recieves an incoming request, look at the
 * data, figures out the response and then send it back.
 *
 */
var this_i = 0;
var this_j=0;
const SplClient = require('./splClient.js');

class Bot {
    constructor(host, port) {
        this.splClient = new SplClient(host, port, this.request.bind(this));
    }

    start() {
        this.splClient.listen();
        console.log("start called");
    }

    /*
     * Entry point for each incoming request from SBOX
     *
     */
    request(data) {
        try {
            switch(data.dataType.trim()) {
                case "authentication" :
                    this.authentication(data);
                    break;
                case "command" :
                    this.move(data);
                    break;
                case "acknowledge" :
                    this.acknowledgement(data);
                    break;
                case "result" :
                    this_i =0;
                    this_j=0;
                    this.result(data);
                    break;
            }
        } catch (err) {
            console.error("Error processing request");
            console.error(err);
        }
    }

    authentication(data) {
        // Send back the one-time-password that
        // was received in the request.
        let response = {
            dataType: 'oneTimePassword',
            oneTimePassword: data.oneTimePassword,
        };
        this.splClient.respond(response);
    }

    acknowledgement(data) {
        console.log("Ack :: status :", data.acknowledge);
        console.log("Ack :: reason :", data.reason);
        
    }

    result(data) {
        console.log("Game over ::", data.gameResult);
    }

    move(request) {
        //console.log("Request move:", request.boardSize);
        console.log("Board Size ::", request.boardSize);
        console.log("Board status ::", request.boardStatus);
        console.log("Error Count :",request.errCount);
        console.log("Player :",request.player);
        console.log("Player 1 points:", request.p1Points);
        console.log("Player 2 points:", request.p2Points);
        console.log("Turn count:", request.turnCount);
        //
        // ** Move logic here. **
        //
        
        let response = {"move":logic(request),"dataType":"response"};

        console.log("Respond move:", response);
        this.splClient.respond(response);
    }
}

var i=0;
function logic(request)
{   if(this_i>=request.boardSize-1) this_i=0;
    if(this_j>=request.boardSize-1) this_j=0;
    var me=request.player, start, stop;
    // for(var i=0;i<request.boardSize;i++)
    // {   
    //     var count=0;var last_J;
    //     for(var j=0;j<request.boardSize;j++)
    //     {
    //         if(request.boardStatus[i][j]==me) //return ((i*10)+j+1).toString();
    //         {
    //             count++;last_J=j;
    //         }
    //     }
    //     if(count>=3) return (((i)*10)+last_J+1+1).toString();
    //     else if(count>=2) { }
    // }
    //for(var i=this_i;i<request.boardSize;i++)
    // {   this_i=i;
        
    //     for(var j=this_j;j<request.boardSize;j++)
    //     {   

    //         this_j=j;
    //         if(this_j==request.boardSize) this_j=0;
    //         if(request.boardStatus[i][j]=='E'){ console.log("if called"); return ((i*10)+j+1).toString();}
    //         // else if(request.boardStatus[i][j]== me){
    //         //     // while(request.boardStatus[i][j]== me) j++;
    //         //     // //return ((i*10)+j+1).toString();
    //         //     // return check(request.boardSize,request.boardStatus,i,j);
    //         //     continue;
    //         // }
    //         console.log(i,j);
           
    //         //else return check(request.boardSize,request.boardStatus,i,j);
    //         // break;
    //     }
    // }
    if (me=='P1') var opp='P2';
    else var opp='P1';
    for(var i=0;i<request.boardSize;i++)
    {   var count=0;
        for(var j=0;j<request.boardSize;j++)
        { 

            // h 0111
            if(j-1 >= 0 && j+2<request.boardSize &&request.boardStatus[i][j-1]=='E'&& request.boardStatus[i][j]==opp && request.boardStatus[i][j+1]==opp && request.boardStatus[i][j+2]==opp ) 
            {
                return ((i*10)+j+1-1).toString();
            }
            //v 0111
            else if(i-1 >= 0  && i+2<request.boardSize && request.boardStatus[i-1][j]=='E' && request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j]==opp && request.boardStatus[i+2][j]==opp) 
            {
                return (((i-1)*10)+j+1).toString();
            }
            //h blocking
            if(j+3<request.boardSize&&request.boardStatus[i][j]==opp && request.boardStatus[i][j+1]==opp && request.boardStatus[i][j+2]==opp && request.boardStatus[i][j+3]=='E') 
            {
                return ((i*10)+j+1+3).toString();
            }
            //v blocking
            else if(i+3<request.boardSize && request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j]==opp && request.boardStatus[i+2][j]==opp && request.boardStatus[i+3][j]=='E') 
            {
                return (((i+3)*10)+j+1).toString();
            }
            // \ blocking
            else if(i+3<request.boardSize && j+3<request.boardSize&&request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j+1]==opp && request.boardStatus[i+2][j+2]==opp && request.boardStatus[i+3][j+3]=='E') 
            {
                return (((i+3)*10)+j+1+3).toString();
            }
            // E\ blocking
            else if(i-1>= 0 && j-1 >= 0 && j+2 <request.boardSize && i+2 <request.boardSize && request.boardStatus[i-1][j-1]=='E'&&request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j+1]==opp && request.boardStatus[i+2][j+2]==opp ) 
            {
                return (((i-1)*10)+j+1-1).toString();
            }
            // / blocking
            else if(i+3<request.boardSize && j-3>=0 &&request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j-1]==opp && request.boardStatus[i+2][j-2]==opp && request.boardStatus[i+3][j-3]=='E') 
            {
                return (((i+3)*10)+j+1-3).toString();
            }
            // /E blocking
            else if(i-1 >= 0 && j+1<request.boardSize && i+2<request.boardSize && j-2>=0 && request.boardStatus[i-1][j+1]=='E'&&request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j-1]==opp && request.boardStatus[i+2][j-2]==opp ) 
            {
                return (((i-1)*10)+j+1+1).toString();
            }
            // 1101 blocking
            if(j+3<request.boardSize&&request.boardStatus[i][j]==opp && request.boardStatus[i][j+1]==opp && request.boardStatus[i][j+2]== 'E' && request.boardStatus[i][j+3]==opp) 
            {
                return ((i*10)+j+1+2).toString();
            }
            //V 1101 blocking
            else if(i+3<request.boardSize && request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j]==opp && request.boardStatus[i+2][j]=="E" && request.boardStatus[i+3][j]== opp) 
            {
                return (((i+2)*10)+j+1).toString();
            }
            // 1011 blocking
            else if(j+3<request.boardSize&&request.boardStatus[i][j]==opp && request.boardStatus[i][j+1]=="E" && request.boardStatus[i][j+2]== opp && request.boardStatus[i][j+3]==opp) 
            {
                return ((i*10)+j+1+1).toString();
            }
            //v 1011 blocking
            else if(i+3<request.boardSize && request.boardStatus[i][j]==opp &&request.boardStatus[i+1][j]=='E' && request.boardStatus[i+2][j]==opp && request.boardStatus[i+3][j]== opp) 
            {
                return (((i+1)*10)+j+1).toString();
            }
            // h me 
            else if(j+3<request.boardSize&&request.boardStatus[i][j]==me && request.boardStatus[i][j+1]==me && request.boardStatus[i][j+2]== me && request.boardStatus[i][j+3]=='E') 
            {
                return ((i*10)+j+1+3).toString();
            }
            // V me 
            else if(i+3<request.boardSize && request.boardStatus[i][j]== me &&request.boardStatus[i+1][j]== me && request.boardStatus[i+2][j]== me && request.boardStatus[i+3][j]=='E') 
            {
                return (((i+3)*10)+j+1).toString();
            }
            // \ me 
            else if(i+3<request.boardSize && j+3<request.boardSize&&request.boardStatus[i][j]== me &&request.boardStatus[i+1][j+1]== me && request.boardStatus[i+2][j+2]== me && request.boardStatus[i+3][j+3]=='E') 
            {
                return (((i+3)*10)+j+1+3).toString();
            }
            // / me 
            else if(i+3<request.boardSize && j-3>=0 &&request.boardStatus[i][j]== me &&request.boardStatus[i+1][j-1]== me && request.boardStatus[i+2][j-2]== me && request.boardStatus[i+3][j-3]=='E') 
            {
                return (((i+3)*10)+j+1-3).toString();
            }
            // 1101 me
            else if(j+3<request.boardSize&&request.boardStatus[i][j]== me && request.boardStatus[i][j+1]== me && request.boardStatus[i][j+2]== 'E' && request.boardStatus[i][j+3]== me) 
            {
                return ((i*10)+j+1+2).toString();
            }
            //V 1101 me
            else if(i+3<request.boardSize && request.boardStatus[i][j]==me &&request.boardStatus[i+1][j]==me && request.boardStatus[i+2][j]=="E" && request.boardStatus[i+3][j]== me) 
            {
                return (((i+2)*10)+j+1).toString();
            }
            // 1011 me
            else if(j+3<request.boardSize&&request.boardStatus[i][j]== me && request.boardStatus[i][j+1]=="E" && request.boardStatus[i][j+2]== me && request.boardStatus[i][j+3]== me) 
            {
                return ((i*10)+j+1+1).toString();
            }
            //v 1011 me
            else if(i+3<request.boardSize && request.boardStatus[i][j]==me &&request.boardStatus[i+1][j]=='E' && request.boardStatus[i+2][j]==me && request.boardStatus[i+3][j]== me) 
            {
                return (((i+1)*10)+j+1).toString();
            }
             // 1001 me
            else if(j+3<request.boardSize&&request.boardStatus[i][j]== me && request.boardStatus[i][j+1]=="E" && request.boardStatus[i][j+2]== 'E' && request.boardStatus[i][j+3]== me) 
             {
                 return ((i*10)+j+1+1).toString();
             }
            // E\ me
            else if(i-1>= 0 && j-1 >= 0 && j+2 <request.boardSize && i+2 <request.boardSize && request.boardStatus[i-1][j-1]=='E'&&request.boardStatus[i][j]==me &&request.boardStatus[i+1][j+1]== me && request.boardStatus[i+2][j+2]== me) 
            {
                return (((i-1)*10)+j+1-1).toString();
            }
            // /E me
            else if(i-1 >= 0 && j+1<request.boardSize && i+2<request.boardSize && j-2>=0 && request.boardStatus[i-1][j+1]=='E'&&request.boardStatus[i][j]==me &&request.boardStatus[i+1][j-1]== me && request.boardStatus[i+2][j-2]== me ) 
            {
                return (((i-1)*10)+j+1+1).toString();
            }
            // h 0111 me
            if(j-1 >= 0 && j+2<request.boardSize &&request.boardStatus[i][j-1]=='E'&& request.boardStatus[i][j]==me && request.boardStatus[i][j+1]==me && request.boardStatus[i][j+2]==me ) 
            {
                return ((i*10)+j+1-1).toString();
            }
            //v 0111 me
            else if(i-1 >= 0  && i+2<request.boardSize && request.boardStatus[i-1][j]=='E' && request.boardStatus[i][j]==me &&request.boardStatus[i+1][j]==me && request.boardStatus[i+2][j]==me) 
            {
                return (((i-1)*10)+j+1).toString();
            }
           
        }
         
    }

    for(var i=this_i;i<request.boardSize;i++)
    {   this_i=i;
        
        for(var j=this_j;j<request.boardSize;j++)
        {   

            this_j=j;

            console.log(i,j);
            // var result = check(request.boardSize,request.boardStatus,i,j);
            // console.log(result);
            if(request.boardStatus[i][j]=='E'){ console.log("if called"); return ((i*10)+j+1).toString();}
            // else if(request.boardStatus[i][j] == "M"){
            //     if(request.boardStatus[i][j+1]== "E") {
            //         this_i = i;
            //         this_j = j+1;
            //         return  (((i)*10)+j+1+1).toString();
            //     } 
            // }
            else if(request.boardStatus[i][j] != me && request.boardStatus[i][j] != "E") {
                // if(request.boardStatus[i+1][j]== "M") {
                //     if(request.boardStatus[i+2][j]== "E")
                //     {this_i = i+2;
                //     this_j = j;
                //     return  (((i+2)*10)+j+1).toString();}
                // }
                console.log("inside blockers")
                if(i+1<request.boardSize && request.boardStatus[i+1][j]== "E") {
                    this_i = i+1;
                    this_j = j;
                    console.log("1")
                    return  (((i+1)*10)+j+1).toString();
                } 
                else if(i+1<request.boardSize && j+1<request.boardSize &&request.boardStatus[i+1][j+1]== "E") {
                    this_i = i+1;
                    this_j = j+1;
                    console.log("2")
                    return  ((((i+1)*10)+j+1+1).toString());
                }  
                if(i==9||j==9)
                {
                    this_i=0;
                    this_j=0;
                }
            }

            //else return ((parseInt(Math.random()*request.boardSize)*10)+parseInt(Math.random()*request.boardSize)+1).toString();
           
            //return result;
        }
    }
    //var i=parseInt(Math.random()*request.boardSize),j=parseInt(Math.random()*request.boardSize);
}

function check(size,status,i,j)
 {		
	// if(received_data[i-1]=='-1' || received_data[i-1]=='-1\r\n') i=i;
	// else { i=parseInt(Math.random()*101);i= check(i,received_data); }

    // return i;
    if(status[i][j]=='E'){return ((i*10)+j+1).toString();}
    else {
          
          console.log("check",i,j);
          var result = check(size,status,i,j+1); 
            return result;
        }
    
}



module.exports = Bot;
